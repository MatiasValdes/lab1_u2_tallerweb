# Lab1_U2_TallerWeb



## ¿Que se encuentra?

Se encuentran 4 carpetas correspondientes a cada uno de los ejercicios, en el caso de la carpeta numero 3, se encuentran 2 subcarpetas las cuales contienen una con metodo get y otra con metodo post. Además, se trabaja con un único archivo css para todos los ejercicios


## ¿Como ejecutar?

Se debe estar en la carpeta "public_html" y tener el modulo de apache disponible. Allí, se podrá ejecutar cada uno de los ejercicios, en el caso del ejercicio 4 se podrán agregar más fotos y serán agregadas a cada una de las columnas

