<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../miestilo.css">
    <title>Tabla de NxN</title>
</head>

<body>
    <h1 id="titulo">TABLA DEL NxN</h1>
    <table id="tabla" ; border=1>
        <?php
    $num = 1;
    // Definición del tamaño de la lista
    define('tam', 25);
    // Ciclo con la constante definida como limite
    for($inicio = 1; $inicio<=tam; $inicio++){
      if($inicio % 2 == 0){
        echo "<tr bgcolor = gray>";
      }
      else{
        echo "<tr bgcolor = white>";
      }
      for($final = 1;$final<=tam;$final++){
        echo "<td>". $num. "</td>";
        $num = $num + 1;
      }
      echo ("</tr>");
    }
    // Impresión del número para la persona
    echo ("La constante definida es ". tam)
    ?>
    </table>
</body>

</html>