<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../miestilo.css">
    <title>Fotos en 4 columnas</title>
</head>

<body>
    <h1>Fotos en 4 columnas</h1>
    <p id="texto"> Se mostrarán las fotos en la carpeta fotos, en 4 columnas</p>
    <?php
    // Asignación del nombre del directorio a una variable
		$dir = "fotos"; 
    // Validar el directorio antes de empezar
		if (is_dir($dir)){ 
      // Variable que guardará el contenido del directorio
			if($fotos = opendir($dir)){ 
				echo "<table id='ej4'>";
				echo "<tr id='filas'>";
        // Variable que será de ayuda para mantener las 4 columnas
				$i=0;
				// Ciclo que leerá las fotos almacenadas
				while (false !== ($archivo = readdir($fotos))){
          // Validar que sea foto, ya que readdir entregaba nombres con puntos vacios
					if ($archivo!="." && $archivo!=".."){
						// Si ya se imprimieron 4 fotos, se pasa a la siguiente linea
						if ($i==4){
              // Y se vuelve el contador a 0
							$i=0;
              // Se cierra la fila y se abre una nueva
							echo "</tr>";
							echo "<tr id='filas'>";
						}
            // Aumento de la variable de control
						$i++;
						echo "<td id='filas'>";
						// Impresión de la imagen 
						echo "<img id='foto' src=$dir/$archivo>"; 
						echo "</td>";
					}
				}
				echo "</tr>"; 
				echo "</table>";
        // Cerrar directorio cuando termina de imprimir
				closedir($fotos);
			}
		}
		?>
    </table>
</body>

</html>