<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../miestilo.css">
    <title>Tabla generada</title>
</head>

<body>
    <h1 id="titulo_2">Tabla generada</h1>
    <table id="tabla" ; border=1>
        <?php
            $metodo = $_SERVER["REQUEST_METHOD"];
            $cad_consulta = $_SERVER['QUERY_STRING'];
            $num = 1;
            $tamanio = $_POST["tamanio"];
            $color_1 = $_POST["color_1"];
            $color_2 = $_POST["color_2"];
            // Comprobación de que no entregue valor para la tabla
            if(empty($tamanio)){
              echo("Seleccione un valor valido");
              echo("<br>");
            }else{
                // Ciclo con la constante definida como limite
                for($inicio = 1; $inicio<=$tamanio; $inicio++){
                  if($inicio % 2 == 0){
                    echo "<tr bgcolor = $color_1>";
                  }
                  else{
                    echo "<tr bgcolor = $color_2>";
                  }
                  for($final = 1;$final<=$tamanio;$final++){
                    echo "<td>", $num, "</td>";
                    $num = $num + 1;
                  }
                  echo ("</tr>");
                }
          }
    ?>
        <a href="./index.php">Volver a crear tabla</a>
    </table>
</body>

</html>