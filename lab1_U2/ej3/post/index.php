<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../miestilo.css">
    <title>Elección de Tabla</title>
</head>

<body>
    <h1 id="titulo_2">Cree su Tabla</h1>
    <p id="texto">Se creará usando método POST</p>
    <FORM METHOD="POST" ACTION="procesar_post.php">
        <table style="margin: 0 auto;">
            <tr>
                <td id="nombre" ALIGN="center">Tamaño de tabla: </td>
                <td ALIGN="center" COLSPAN="5"><INPUT TYPE="number" min=1 max=100 NAME="tamanio" SIZE="15"></td>
            </tr>
            <tr>
                <td id="nombre" ALIGN="center">Color N°1: </td>
                <td ALIGN="center"><input type="color" name="color_1" size="15"></td>
            </tr>
            <tr>
                <td id="nombre" ALIGN="center">Color N°2: </td>
                <td ALIGN="center"><input type="color" name="color_2" size="15"></td>
            </tr>
        </table>
        <p style="text-align:center;"><INPUT type="SUBMIT" value="Enviar"> <INPUT type="RESET"></p>
    </FORM>
    <p id="texto">Se recomienda usar colores claros para que sean posibles de visualizar</p>
</body>

</html>