<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../miestilo.css">
    <title>Tabla</title>
</head>

<body>
    <h1>TABLA DEL 1 AL 100</h1>
    <br><br>
    <table id="ej1" ; border=1>
        <?php
    $num = 1;
    // Ciclo for que imprimirá los numeros del 1 al 100
    for($inicio = 1; $inicio<=10; $inicio++){
      echo "<tr id='filas_ej1'>";
      for($final = 1;$final<=10;$final++){
        // Impresión
        echo "<td id='filas_ej1'>". $num. "</td>";
        $num = $num + 1;
      }
      echo "</tr>";
    }
    ?>
    </table>
</body>

</html>